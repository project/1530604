<?php

/**
* @file
* Implements hook_views_default_views()
*/

function content_share_views_default_views() {
// Begin copy and paste of output from the Export tab of a view.

$view = new view;
$view->name = 'content_share_stats';
$view->description = '';
$view->tag = '';
$view->base_table = 'content_share_stats';
$view->human_name = 'content_share_stats';
$view->core = 6;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access administration pages';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'table';
/* Field: Content Share Stats: Query ID */
$handler->display->display_options['fields']['qid']['id'] = 'qid';
$handler->display->display_options['fields']['qid']['table'] = 'content_share_stats';
$handler->display->display_options['fields']['qid']['field'] = 'qid';
$handler->display->display_options['fields']['qid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['qid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['qid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['qid']['alter']['external'] = 0;
$handler->display->display_options['fields']['qid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['qid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['qid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['qid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['qid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['qid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['qid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['qid']['alter']['html'] = 0;
$handler->display->display_options['fields']['qid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['qid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['qid']['hide_empty'] = 0;
$handler->display->display_options['fields']['qid']['empty_zero'] = 0;
$handler->display->display_options['fields']['qid']['hide_alter_empty'] = 1;
/* Field: Content Share Stats: Query */
$handler->display->display_options['fields']['query']['id'] = 'query';
$handler->display->display_options['fields']['query']['table'] = 'content_share_stats';
$handler->display->display_options['fields']['query']['field'] = 'query';
$handler->display->display_options['fields']['query']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['query']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['query']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['query']['alter']['external'] = 0;
$handler->display->display_options['fields']['query']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['query']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['query']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['query']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['query']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['query']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['query']['alter']['trim'] = 0;
$handler->display->display_options['fields']['query']['alter']['html'] = 0;
$handler->display->display_options['fields']['query']['element_label_colon'] = 1;
$handler->display->display_options['fields']['query']['element_default_classes'] = 1;
$handler->display->display_options['fields']['query']['hide_empty'] = 0;
$handler->display->display_options['fields']['query']['empty_zero'] = 0;
$handler->display->display_options['fields']['query']['hide_alter_empty'] = 1;
/* Field: Broken/missing handler */
$handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['table'] = 'content_share_stats';
$handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['label'] = 'Timestamp';
$handler->display->display_options['fields']['timestamp']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['external'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['timestamp']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['timestamp']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['trim'] = 0;
$handler->display->display_options['fields']['timestamp']['alter']['html'] = 0;
$handler->display->display_options['fields']['timestamp']['element_label_colon'] = 1;
$handler->display->display_options['fields']['timestamp']['element_default_classes'] = 1;
$handler->display->display_options['fields']['timestamp']['hide_empty'] = 0;
$handler->display->display_options['fields']['timestamp']['empty_zero'] = 0;
$handler->display->display_options['fields']['timestamp']['hide_alter_empty'] = 1;
/* Field: Content Share Stats: User ID */
$handler->display->display_options['fields']['uid']['id'] = 'uid';
$handler->display->display_options['fields']['uid']['table'] = 'content_share_stats';
$handler->display->display_options['fields']['uid']['field'] = 'uid';
$handler->display->display_options['fields']['uid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['uid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['uid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['uid']['alter']['external'] = 0;
$handler->display->display_options['fields']['uid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['uid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['uid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['uid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['uid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['uid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['uid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['uid']['alter']['html'] = 0;
$handler->display->display_options['fields']['uid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['uid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['uid']['hide_empty'] = 0;
$handler->display->display_options['fields']['uid']['empty_zero'] = 0;
$handler->display->display_options['fields']['uid']['hide_alter_empty'] = 1;
/* Field: Content Share Stats: User IP */
$handler->display->display_options['fields']['ip']['id'] = 'ip';
$handler->display->display_options['fields']['ip']['table'] = 'content_share_stats';
$handler->display->display_options['fields']['ip']['field'] = 'ip';
/* Field: Content Share Stats: Error messages */
$handler->display->display_options['fields']['error']['id'] = 'error';
$handler->display->display_options['fields']['error']['table'] = 'content_share_stats';
$handler->display->display_options['fields']['error']['field'] = 'error';
$handler->display->display_options['fields']['error']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['error']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['error']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['error']['alter']['external'] = 0;
$handler->display->display_options['fields']['error']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['error']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['error']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['error']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['error']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['error']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['error']['alter']['trim'] = 0;
$handler->display->display_options['fields']['error']['alter']['html'] = 0;
$handler->display->display_options['fields']['error']['element_label_colon'] = 1;
$handler->display->display_options['fields']['error']['element_default_classes'] = 1;
$handler->display->display_options['fields']['error']['hide_empty'] = 0;
$handler->display->display_options['fields']['error']['empty_zero'] = 0;
$handler->display->display_options['fields']['error']['hide_alter_empty'] = 1;
/* Sort criterion: Content Share Stats: Query ID */
$handler->display->display_options['sorts']['qid']['id'] = 'qid';
$handler->display->display_options['sorts']['qid']['table'] = 'content_share_stats';
$handler->display->display_options['sorts']['qid']['field'] = 'qid';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page_1');
$handler->display->display_options['path'] = 'content_share_stats';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Content Share Statistics';
$handler->display->display_options['menu']['description'] = 'Queries run via Content Share interface';
$handler->display->display_options['menu']['weight'] = '0';

// End copy and paste of Export tab output.

// Add view to list of views to provide.
  $views[$view->name] = $view;

// ...Repeat all of the above for each view the module should provide.

// At the end, return array of default views.
  return $views;
}
